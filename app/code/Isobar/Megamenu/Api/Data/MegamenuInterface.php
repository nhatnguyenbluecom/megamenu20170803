<?php

namespace Isobar\Megamenu\Api\Data;


interface MegamenuInterface
{
    /**
     * Constants defined for keys of data array
     */
    const TITLE         = 'title';
    const LINK          = 'link';
    const ROOT_ID       = 'root_id';
    const PARENT_ID     = 'parent_id';
    const DESCRIPTION   = 'description';
    const EXTEND_HTML   = 'extend_html';
    const COLOR_CODE   = 'color_code';
    const STATUS        = 'status';
    const SORT          = 'sort';
    const CREATED_AT    = 'created';

    /**
     * Get megamenu id
     * @return int|null
     */
    public function getId();

    /**
     * Set megamenu id
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get megamenu title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set megamenu title
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Set megamenu link
     * @param string $link
     * @return $this
     */
    public function setLink($link);

    /**
     * Get megamenu link
     * @return string|null
     */
    public function getLink();

    /**
     * Set megamenu description
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * Get megamenu description
     * @return string|null
     */
    public function getDescription();

    /**
     * Set megamenu extend_html
     * @param string $extendHtml
     * @return $this
     */
    public function setExtendHtml($extendHtml);

    /**
     * Get megamenu extend_html
     * @return string|null
     */
    public function getExtendHtml();

    /**
     * Set megamenu color code
     * @param string $colorCode
     * @return $this
     */
    public function setColorCode($colorCode);

    /**
     * Get megamenu color code
     * @return string|null
     */
    public function getColorCode();

    /**
     * Set megamenu status
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get megamenu status
     * @return int|null
     */
    public function getStatus();

    /**
     * Set megamenu rootid
     * @param int $rootId
     * @return $this
     */
    public function setRootId($rootId);

    /**
     * Get megamenu root id
     * @return int|null
     */
    public function getRootId();

    /**
     * Set megamenu parent id
     * @param int $parentId
     * @return $this
     */
    public function setParentId($parentId);

    /**
     * Get megamenu parent id
     * @return int|null
     */
    public function getParentId();

    /**
     * Set megamenu sort
     * @param int $sort
     * @return $this
     */
    public function setSort($sort);

    /**
     * Get megamenu sort
     * @return int|null
     */
    public function getSort();

    /**
     * Get megamenu created date
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set megamenu created date
     *
     * @param string $timeStamp
     * @return $this
     */
    public function setCreatedAt($timeStamp);
}