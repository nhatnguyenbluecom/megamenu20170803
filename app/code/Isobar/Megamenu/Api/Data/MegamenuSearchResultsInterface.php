<?php

namespace Isobar\Megamenu\Api\Data;

/**
 * @api
 */
interface MegamenuSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Isobar\Megamenu\Api\Data\MegamenuInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Isobar\Megamenu\Api\Data\MegamenuInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
