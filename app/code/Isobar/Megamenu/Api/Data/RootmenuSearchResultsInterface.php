<?php

namespace Isobar\Megamenu\Api\Data;

/**
 * @api
 */
interface RootmenuSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Isobar\Megamenu\Api\Data\RootmenuInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Isobar\Megamenu\Api\Data\RootmenuInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
