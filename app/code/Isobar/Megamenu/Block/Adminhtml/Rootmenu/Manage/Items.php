<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 13/06/2017
 * Time: 17:55
 */

namespace Isobar\Megamenu\Block\Adminhtml\Rootmenu\Manage;


class Items extends \Magento\Backend\Block\Widget\Form\Container
{

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context
    ) {
        parent::__construct($context);
    }

    public function _construct()
    {
        $rootMenuId = $this->getRequest()->getParam('root_id');
        parent::_construct();
        $this->buttonList->remove('reset');
        $this->buttonList->remove('save');
        $this->buttonList->remove('delete');
        $this->buttonList->add('expand', [
            'label' => __('Expand All'),
            'class' => 'primary flush-cache-magento'
        ]);
        $this->buttonList->add('collapse', [
            'label' => __('Collapse All'),
            'class' => 'primary flush-cache-magento'
        ]);
        $this->buttonList->add(
            'add_menu_item',
            [
                'label' => __('Add Menu Item'),
                'class' => 'action-default scalable add primary',
                'onclick' => "location.href='" . $this->getUrl('*/menu/new', array('root_id' => $rootMenuId)) . "'",
            ]
        );
        $this->_objectId = 'item_id';

        $this->_blockGroup = 'Isobar_Megamenu';
        $this->_controller = 'adminhtml_rootmenu_manage';
    }


    public function getHeaderText()
    {
        return __('Manage Menu Items');
    }
}