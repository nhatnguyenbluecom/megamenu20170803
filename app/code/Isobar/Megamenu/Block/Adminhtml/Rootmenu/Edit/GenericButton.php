<?php
namespace Isobar\Megamenu\Block\Adminhtml\Rootmenu\Edit;

use Magento\Backend\Block\Widget\Context;
use Isobar\Megamenu\Api\RootmenuRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var RootmenuRepositoryInterface
     */
    protected $rootMenuRepository;

    /**
     * @param Context $context
     * @param RootmenuRepositoryInterface $rootMenuRepository
     */
    public function __construct(
        Context $context,
        RootmenuRepositoryInterface $rootMenuRepository
    ) {
        $this->context = $context;
        $this->rootMenuRepository = $rootMenuRepository;
    }

    /**
     * Return Banner ID
     *
     * @return int|null
     */
    public function getId()
    {
        try {
            return $this->rootMenuRepository->get(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
