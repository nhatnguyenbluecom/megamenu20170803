<?php
namespace Isobar\Megamenu\Helper;
use Magento\Framework\App\Config\ScopeConfigInterface;
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfig;
    protected $storeManager;
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;

    }

    public function getMegamenuStoreConfig($key, $encrypt = false)
    {
        $value = $this->scopeConfig->getValue('isobar_mega_config/general/' . $key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($encrypt) {
            return $this->_encryptor->decrypt($value);
        }
        return $value;
    }

    public function getStatus()
    {
        $status = $this->getMegamenuStoreConfig('enabledisable');
        return ($status == 1) ? true : false;
    }
}
