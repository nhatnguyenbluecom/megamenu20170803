<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Menu;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuReposity;

    /**
     * @var \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory
     */
    protected $megaMenuFactory;

    /**
     * Edit constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuReposity
     * @param \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megaMenuFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuReposity,
        \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megaMenuFactory
    ) {
        $this->megaMenuReposity = $megaMenuReposity;
        $this->megaMenuFactory = $megaMenuFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Edit Isobar Mega Menu
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->megaMenuFactory->create();

        if ($id) {
            $model = $this->megaMenuReposity->get($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This menu item no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Isobar_Megamenu::megamenu')->addBreadcrumb(__('Menu Item'), __('Menu Item'))
            ->addBreadcrumb(
            $id ? __('Edit Block') : __('New Menu Item'),
            $id ? __('Edit Block') : __('New Menu Item')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Menu Item'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Menu Item'));
        return $resultPage;
    }
}
