<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Rootmenu;
use Isobar\Megamenu\Model\Rootmenu;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory
     */
    protected $rootMenuFactory;

    /**
     * @var \Isobar\Megamenu\Api\RootmenuRepositoryInterface
     */
    protected $rootMenuRepository;

    /**
     * @var \Isobar\Megamenu\Model\ResourceModel\Rootmenu\CollectionFactory
     */
    protected $rootMenuCollection;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory $rootMenuFactory
     * @param \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuRepository
     * @param \Isobar\Megamenu\Model\ResourceModel\Rootmenu\CollectionFactory $rootMenuCollection
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        DataPersistorInterface $dataPersistor,
        \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory $rootMenuFactory,
        \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuRepository,
        \Isobar\Megamenu\Model\ResourceModel\Rootmenu\CollectionFactory $rootMenuCollection
    ) {
        $this->rootMenuCollection = $rootMenuCollection;
        $this->rootMenuFactory = $rootMenuFactory;
        $this->rootMenuRepository = $rootMenuRepository;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Megamenu::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }

            try {
                $item = $this->rootMenuFactory->create();
                $data['created_at'] = (new \DateTime())->getTimestamp();
                $item->setData($data);
                if ($data['as_root_menu']) {
                    //reset rootmenu by storeview
                    $this->_resetRootMenuByStore($data['store_id']);
                }
                $model = $this->rootMenuRepository->save($item);
                if (!$model->getId() && $id) {
                    $this->messageManager->addError(__('This root menu no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
                $this->messageManager->addSuccess(__('You saved the root menu.'));
                $this->dataPersistor->clear('isobar_rootmenu');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                echo $e->getMessage();die();
                $this->messageManager->addException($e, __('Something went wrong while saving the root menu.'));
            }

            $this->dataPersistor->set('isobar_rootmenu', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Reset root menu by store
     * @param $storeId
     * return void
     */
    protected function _resetRootMenuByStore($storeId)
    {
        $rootMenucollection = $this->rootMenuCollection->create();
        $rootMenucollection->addFieldToFilter('store_id', ['eq' => $storeId]);
        $rootMenucollection->addFieldToFilter('as_root_menu', ['eq' => 1]);
        foreach($rootMenucollection as $rootMenu) {
            $rootMenu->setAsRootMenu(0);
            $rootMenu->save();
        }
    }
}
