<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Rootmenu;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Isobar\Megamenu\Api\RootmenuRepositoryInterface
     */
    protected $rootMenuReposity;

    /**
     * @var \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory
     */
    protected $rooMenuFactory;

    /**
     * Edit constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuReposity
     * @param \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory $rootMenuFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuReposity,
        \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory $rootMenuFactory
    ) {
        $this->rootMenuReposity = $rootMenuReposity;
        $this->rootMenuFactory = $rootMenuFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Edit Isobar Mega Menu
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->rootMenuFactory->create();

        if ($id) {
            $model = $this->rootMenuReposity->get($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This root menu no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Isobar_Megamenu::megamenu')->addBreadcrumb(__('Root Menu'), __('Root Menu'))
            ->addBreadcrumb(
            $id ? __('Edit Block') : __('New Root Menu'),
            $id ? __('Edit Block') : __('New Root Menu')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Root Menu'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Root Menu'));
        return $resultPage;
    }
}
