<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Rootmenu;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory
     */
    protected $rootMenuFactory;

    /**
     * @var \Isobar\Megamenu\Api\RootmenuRepositoryInterface
     */
    protected $rootMenuReposity;

    /**
     * Index constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory $rootMenuFactory
     * @param \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuReposity
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Isobar\Megamenu\Api\Data\RootmenuInterfaceFactory $rootMenuFactory,
        \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuReposity
    ) {
        $this->rootMenuFactory= $rootMenuFactory;
        $this->rootMenuReposity = $rootMenuReposity;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Isobar_Megamenu::megamenu')->addBreadcrumb(__('Root Mega Menus'), __('Root Mega Menus'));
        $resultPage->getConfig()->getTitle()->prepend(__('Root Mega Menus'));
        return $resultPage;
    }
}
